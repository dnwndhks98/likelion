package com.example.liklion_project_2022_12_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiklionProject20221220Application {

    public static void main(String[] args) {
        SpringApplication.run(LiklionProject20221220Application.class, args);
    }

}
